var lastID = 'thread_255788';
var interval = 30; // sec

var fShouted = false;
var audio;
var timer;

$(document).ready(function(){
    audio = document.createElement('audio');
    audio.setAttribute('src', 'alert.mp3');

    start();
});

function request () {
    var request = $.ajax({
        url: "get.php",
        type: "GET"
    });     

    request.done(function (response){
        var div = document.createElement('div');
        div.innerHTML = response;

        var lines = $('.threadbitX', $(div));

        for ( var i = lines.length - 1; i >= 0; i -- ) {
            var id = $(lines[i]).context.id;
            if ( id > lastID ) {
                if ( ! fShouted ) {
                    audio.play();
                    fShouted = true;
                    $('body > div').html('')
                }

                lastID = id;

                $('body > div').append(lines[i]);

                var a = $('#'+id+' .inner h3 a');
                a.attr('href', 'https://webtrh.cz/'.concat(a.attr('href')));
            }
        }

        fShouted = false;

        div = null;

        console.log('last load at ' + Date().split(' ')[4]);
    });
    
    request.fail(function (xhr, statusText, thrownError){
        $('body').html("Vyskytla se chyba: " + statusText);
        audio.play();
    });
}

function stop () {
    clearInterval(timer);
    console.log('Stoped watching.');
}

function start () {
    request();
    timer = window.setInterval(function(){ request() }, interval * 1000);
}